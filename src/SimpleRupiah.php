<?php
 
namespace cacopa\rupiah;
 
/**
 * Basic Calculator.
 * 
 */
class SimpleRupiah
{
    /**
     * Menjumlahkan semua data dalam sebuah array.
     *
     * @param int angka
     * @return string rupiah
     */
    public static function rp_format($angka){
        $angka = (int) $angka;
        return number_format($angka,0,',','.');
    }
    
    /**
     * Menjumlahkan semua data dalam sebuah array.
     *
     * @param int angka
     * @return string rupiah
     */
    public static function rupiah_terbilang($x)
    {
      $x = round($x);
      $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
      if ($x < 12)
        return " " . $abil[$x];
      elseif ($x < 20)
        return self::rupiah_terbilang($x - 10) . "belas";
      elseif ($x < 100)
        return self::rupiah_terbilang($x / 10) . " puluh" . self::rupiah_terbilang($x % 10);
      elseif ($x < 200)
        return " seratus" . self::rupiah_terbilang($x - 100);
      elseif ($x < 1000)
        return self::rupiah_terbilang($x / 100) . " ratus" . self::rupiah_terbilang($x % 100);
      elseif ($x < 2000)
        return " seribu" .self::rupiah_terbilang($x - 1000);
      elseif ($x < 1000000)
        return self::rupiah_terbilang($x / 1000) . " ribu" . self::rupiah_terbilang($x % 1000);
      elseif ($x < 1000000000)
        return self::rupiah_terbilang($x / 1000000) . " juta" . self::rupiah_terbilang($x % 1000000);
    }
}